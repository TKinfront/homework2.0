// Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// AJAX - це технологія, яка дозволяє оновлювати вміст веб-сторінки без її перезавантаження
// та зменшує кількість даних, які передаються між браузером та сервером.


// Завдання
// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:

// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни

// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.


// Необов'язкове завдання підвищеної складності

// Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.



const link = "https://ajax.test-danit.com/api/swapi/films";
const container = document.querySelector("#container");

const getCharacter = (urlCharacter, id) => {
  fetch(urlCharacter)
    .then(response => response.json())
    .then(res => {
      document.querySelector(`#${id}`).innerHTML += `<p>${res.name}</p>`;
    })
    .catch(error => console.error(error));
};

fetch(link)
  .then(response => response.json())
  .then(res => {
    console.log(res);
    res.forEach((element, index) => {
      container.innerHTML += `<p class="h1">${element.episodeId}</p>
        <p class="h2">${element.name}</p>
        <img id="loading-${index}" src="Spin.gif"/>
        <p class="h3">${element.openingCrawl}</p>
        <p class="characters" id="character-${index}"></p>`;
      element.characters.forEach(character => {
        getCharacter(character, `character-${index}`);
      });
      setTimeout(() => {
        document.querySelector(`#loading-${index}`).style.display = "none"
    }, 2000);
    });
  })
  .catch(error => console.error(error));