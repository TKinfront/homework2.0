// Теоретичне питання
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

// Замість очікування завершення виконання послідовних операцій, асинхронність дозволяє виконувати код паралельно без їх блокування.

const linkGetIP = "https://api.ipify.org/?format=json";
const linkGetLocation = "http://ip-api.com/json/";
const parameters = "?fields=status,message,continent,country,region,city,district,query";
const buttonGetIP = document.getElementById("button");
const ul = document.createElement("ul");
buttonGetIP.insertAdjacentElement("afterend", ul);

const generateInfo = (field) => {
    const infoItems = [
        { label: "Континент", value: field.continent },
        { label: "Країна", value: field.country },
        { label: "Регіон", value: field.region },
        { label: "Місто", value: field.city },
        { label: "Район", value: field.district }
    ];

    ul.innerHTML = infoItems.map(item => `<li>${item.label}: ${item.value ? item.value : "No data!"}</li>`).join("");
};

const getIP = async () => {
    try {
        const res = await axios.get(linkGetIP);
        const ip = res.data.ip;
        await getLocation(ip);
    } catch (error) {
        console.error("Didn't get the IP", error);
    }
};

const getLocation = async (userIp) => {
    try {
        const res = await axios.get(`${linkGetLocation}${userIp}${parameters}`);
        generateInfo(res.data);
    } catch (error) {
        console.error("Location is unavailable", error);
    }
};

buttonGetIP.addEventListener("click", () => {
    getIP();
});