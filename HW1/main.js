// ## Теоретичне питання
// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?

// 1. Прототипне наслідування в JS - це механізм, що дозволяє створювати нові об'єкти на основі інших об'єктів-прототипів.
// Кожен об'єкт у JS має прототип, який може бути використаний для успадкування їх властивостей та методів.

// 2. Виклик super() у конструкторі класу-нащадка є необхідним для успадкування та ініціалізації властивостей класу-батька в класі-нащадкa.




// ## Завдання
// 1. Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// 2. Створіть гетери та сеттери для цих властивостей.
// 3. Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// 4. Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// 5. Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

// ## Примітка
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Література:
// - [Класи на MDN](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Classes)
// - [Класи в ECMAScript 6](https://frontender.info/es6-classes-final/)


class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set name(name) {
        this._name = name;
    }
    get name() {
        return this._name;
    }

    set age(age) {
        this._age = age;
    }
    get age() {
        return this._age;
    }

    set salary(salary) {
        this._salary = salary;
    }
    get salary() {
        return this._salary;
    }
}

let employee = new Employee("TK", 20, 500)
employee.salary = "1000"

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
    set lang(lang) {
        this._lang = lang;
    }
    get lang() {
        return this._lang;
    }

    get salary() {
        return this._salary * 3
    }
}

let programmers = [
    new Programmer("Mask", 40, 1500),
    new Programmer("Jeffrey", 50, 2000),
    new Programmer("Scott", 55, 2500)
]

for (let programmer of programmers) {
    console.log(`Name: ${programmer.name},`, `Age: ${programmer.age},`, `Salary: ${programmer.salary}`);
}