// Теоретичне питання.
// Наведіть кілька прикладів, коли доречно використовувати в коді JS конструкцію try...catch.

// Конструкція try...catch використовується в JS для обробки помилок, які виникають в процесі виконання коду.
// Наприклад, під час роботи з об'єктами можуть виникати помилки, коли властивість не існує або не має прав на доступ до неї.
// Також при взаємодії з мережею, коли сервер недоступний, можна обробляти помилки і виводити користувачеві зрозуміле повідомлення про те, що сталося.




// Завдання.
// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];


const rootContainer = document.getElementById("root");
rootContainer.innerHTML = `<ul id="books"></ul>`;
const ulElement = document.getElementById("books");


books.forEach((book, index) => {
    ulElement.innerHTML += `<li class="li-element" id="liElement${index}"></li>`
    const liElement = document.getElementById(`liElement${index}`);
    try {
        if (book.author) {
            liElement.innerHTML += `<p>${book.author}</p>`
        } else {
            console.log(`This book "${book.name}" has no author.`);
        }
    } catch (e) {
        console.log(e);
    }

    try {
        if (book.name) {
            liElement.innerHTML += `<p>${book.name}</p>`
        } else {
            console.log(`This book has undefined name.`);
        }
    } catch (e) {
        console.log(e);
    }

    try {
        if (book.price) {
            liElement.innerHTML += `<p>${book.price}</p>`
        } else {
            console.log(`This book "${book.name}" has no price.`);
        }
    } catch (e) {
        console.log(e);
    }
})
