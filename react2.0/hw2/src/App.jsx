import React from 'react';
import axios from 'axios'
import Main from './components/Main/Main';
import Modal from './components/Modal/Modal';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import ModalWishList from './components/Modal/ModalWishList';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalBuyOpen: false,
      isModalWishListOpen: false,
      products: [],
      arrWishList: [],
      basket: 0,
      wishList: 0
    };
  }

  componentDidMount() {
    axios("/collection.json").then(res => {
      this.setState({ ...this.state, products: [...res.data] });
    })
  }

  render() {

    const buttons = {
      headerBuyModal: "Зробіть Ваш вибір.",
      headerWishListModal: "Список обраних товарів.",
      bodyBuyModal: "Додати до кошику?",
      bodyWishListModal: "Додати до обраних?"
    }
    const openBuyModal = () => {
      this.setState({
        ...this.state,
        isModalBuyOpen: this.state.isModalBuyOpen === false ? true : false
      })
    }

    const openWishListModal = () => {
      this.setState({
        ...this.state,
        isModalWishListOpen: this.state.isModalWishListOpen === false ? true : false
      })
    }

    const confirmOrder = () => {
      localStorage.setItem("basket", this.state.basket + 1)
      this.setState({
        ...this.state,
        basket: this.state.basket + 1,
        isModalBuyOpen: this.state.isModalBuyOpen === false ? true : false
      })
    }

    const confirmWishList = () => {
      localStorage.setItem("wishList", this.state.wishList + 1)
      this.setState({
        ...this.state,
        wishList: this.state.wishList + 1,
        isModalWishListOpen: this.state.isModalWishListOpen === false ? true : false
      })
    }
    return (
      <>
        <div className="app">
          <Header
          wishList={this.state.wishList}
          basket={this.state.basket}
          />
          <Main
            items={this.state.products}
            openBuyModal={openBuyModal}
            openWishListModal={openWishListModal}
            confirmWishList={confirmWishList}


          />
          <Footer/>
          {this.state.isModalBuyOpen === true ? <Modal buttons={buttons} openBuyModal={openBuyModal} confirmOrder={confirmOrder} /> : null}
          {this.state.isModalWishListOpen === true ? <ModalWishList buttons={buttons} openWishListModal={openWishListModal} confirmWishList={confirmWishList} /> : null}
        </div>
      </>
    );
  }
}

export default App;