import style from "./Header.module.scss"
import basket from "./shopping-cart.png"
import wishList from "./wishlist.png"

const Header = (props) => {
    return (
        <div className={style.container}>
            <div className={style.container_icons}>
                <a className={style.container_icons_wishList}>
                    <img src={wishList} alt="wishList"/>
                    <div className={style.container_icons_wishList_counter}>{props.wishList}</div>
                </a>
                <a className={style.container_icons_basket}>
                    <img src={basket} alt="basket"/>
                    <div className={style.container_icons_basket_counter}>{props.basket}</div>
                </a>
            </div>
        </div>
    )
}
export default Header;