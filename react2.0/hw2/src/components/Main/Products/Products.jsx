import style from "./Products.module.scss"
import ButtonBuy from "../../Button/ButtonBuy";
import ButtonWishList from "../../Button/ButtonWishList";

const Products = (props) => {
    return (
        <div className={style.products}>
            <a className={style.linkContainer}>
                <img className={style.picture} src={props.picture} />
                <ButtonWishList openWishListModal={props.openWishListModal} id={props.id}/>
            </a>
            <div className={style.description}>
                <p className={style.productName}>{props.name}</p>
                <p>Код товару: {props.id}</p>
                <p>{props.lang}</p>
                <p>{props.price} грн.</p>
                <ButtonBuy openBuyModal={props.openBuyModal} />
            </div>
        </div>
    )
}

export default Products;