import style from "./Modal.module.scss";

const ModalWishList = (props) => {
    console.log(props);
    return (
      <div className={style.modal} onClick={props.openWishListModal}>
        <div className={style.modalContent} onClick={(event) => {event.stopPropagation()}}>
          <div className={style.modalHeader}>
            <h2>{props.buttons.headerWishListModal}</h2>
          </div>
          <div className={style.modalBody}>
            <p>{props.buttons.bodyWishListModal}</p>
          </div>
          <div className={style.modalActions}>
            <button className={style.buttonConfirm} onClick={props.confirmWishList}>Підтвердити</button>
            <button className={style.buttonClose} onClick={props.openWishListModal}>Назад</button>
          </div>
        </div>
      </div>
    );
  }
  
  
  export default ModalWishList;