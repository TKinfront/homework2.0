
import style from "./Modal.module.scss";

const Modal = (props) => {
  return (
    <div className={style.modal} onClick={props.openBuyModal}>
      <div className={style.modalContent} onClick={(event) => {event.stopPropagation()}}>
        <div className={style.modalHeader}>
          <h2>{props.buttons.headerBuyModal}</h2>
        </div>
        <div className={style.modalBody}>
          <p>{props.buttons.bodyBuyModal}</p>
        </div>
        <div className={style.modalActions}>
          <button className={style.buttonConfirm} onClick={props.confirmOrder}>Підтвердити</button>
          <button className={style.buttonClose} onClick={props.openBuyModal}>Назад</button>
        </div>
      </div>
    </div>
  );
}


export default Modal;