import style from "./ButtonBuy.module.scss"

const ButtonBuy = (props) => {

  return (
    <button className={style.button} onClick={props.openBuyModal} type="button">
    Купити
    </button>
  );
}


export default ButtonBuy;