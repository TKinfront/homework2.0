import React from 'react';
import { Provider } from 'react-redux';
import store from './redux/store';
import ReactDOM from 'react-dom/client';
import './reset.css';
import './index.css';
import reportWebVitals from './reportWebVitals';
import AppContainer from './AppContainer';

const root = ReactDOM.createRoot(document.getElementById('root'));

const setFirstStorage = (key) => {
  if (!localStorage.getItem(key)) {
      localStorage.setItem(key, JSON.stringify([]))
  }
}
setFirstStorage('arrBasketListLocal')
setFirstStorage('basketLocal')
// localStorage.setItem("arrBasketListLocal", "")
// localStorage.setItem("basketLocal", "0")

root.render(
  <Provider store={store}>
    <AppContainer />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
