import types from "./types";


const initialState = {
    isModalBuyOpen: false,
    isModalWishListOpen: false,
    products: [],
    arrWishList: JSON.parse(localStorage.getItem('wishList')) || [],
    arrBasketList: JSON.parse(localStorage.getItem('arrBasketListLocal')) || [],
    basket: JSON.parse(localStorage.getItem('basketLocal')) || 0,
    wishList: 0,
    wishCandidate: null,
    basketCandidate: null
}

const mainReducer = (state = initialState, action) => {

    switch (action.type) {
        case types.OPEN_BUY_MODAL:
            return {
                ...state,
                isModalBuyOpen: state.isModalBuyOpen === false ? true : false,
                basketCandidate: action.idCandidate
            }
        case types.OPEN_WISHLIST_MODAL:
            return {
                ...state,
                isModalWishListOpen: state.isModalWishListOpen === false ? true : false,
                wishCandidate: action.idCandidate
            }
        case types.CONFIRM_ORDER:
            localStorage.setItem('arrBasketListLocal', JSON.stringify([
                ...state.arrBasketList,
                state.basketCandidate
            ]));
            localStorage.setItem('basketLocal', state.basket + 1)
            return {
                ...state,
                basket: state.basket + 1,
                isModalBuyOpen: state.isModalBuyOpen === false ? true : false,
                arrBasketList: [...state.arrBasketList, state.basketCandidate],
            };

        case types.FINISH_ORDER:
            localStorage.setItem('arrBasketListLocal', JSON.stringify([]));
            localStorage.setItem('basketLocal', 0);
            localStorage.setItem('basket', 0);
            localStorage.removeItem('basketCandidate');
            return {
                ...state,
                basket: 0,
                arrBasketList: [],
                basketCandidate: null,
            }

        case types.CONFIRM_WISHLIST:
            return {
                ...state,
                wishList: state.wishList + 1,
                isModalWishListOpen: state.isModalWishListOpen === false ? true : false,
                arrWishList: [...state.arrWishList, localStorage.getItem("wishCandidate")]
            }
        case types.CHANGE_FAV:
            const { arrWishList } = state
            let newWishList
            if (arrWishList.includes(action.id)) {
                newWishList = arrWishList.filter(wishId => wishId !== action.id);
            } else {
                newWishList = [...arrWishList, action.id];
            }
            localStorage.setItem('wishList', JSON.stringify(newWishList));
            return {
                ...state,
                arrWishList: newWishList
            }
        case types.GET_PRODUCTS:
            return {
                ...state,
                products: [...action.products]
            }
        default:
            return state;
    }
}

export const openBuyModal = (idCandidate) => ({
    type: types.OPEN_BUY_MODAL,
    idCandidate
});

export const openWishListModal = (idCandidate) => ({
    type: types.OPEN_WISHLIST_MODAL,
    idCandidate
});

export const confirmOrder = () => ({
    type: types.CONFIRM_ORDER
});

export const confirmWishList = () => ({
    type: types.CONFIRM_WISHLIST
});

export const changeFav = (id) => ({
    type: types.CHANGE_FAV,
    id
});

export const getProducts = (products) => ({
    type: types.GET_PRODUCTS,
    products
});

export const finishOrder = () => ({
    type: types.FINISH_ORDER
})

export default mainReducer;