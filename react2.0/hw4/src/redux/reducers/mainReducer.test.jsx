import mainReducer, {
    openBuyModal,
    openWishListModal,
    confirmOrder,
    changeFav,
    getProducts,
    finishOrder
} from "./mainReducer";

const initialState = {
    isModalBuyOpen: false,
    isModalWishListOpen: false,
    products: [],
    arrWishList: JSON.parse(localStorage.getItem('wishList')) || [],
    arrBasketList: JSON.parse(localStorage.getItem('arrBasketListLocal')) || [],
    basket: JSON.parse(localStorage.getItem('basketLocal')) || 0,
    wishList: 0,
    wishCandidate: null,
    basketCandidate: null
}

describe("mainReducer", () => {
    it('openBuyModal', () => {
        const action = openBuyModal();
        const newState = mainReducer(initialState ,action);
        expect(newState.isModalBuyOpen).toBe(true);
    })
    it('openWishListModal', () => {
        const action = openWishListModal();
        const newState = mainReducer(initialState ,action);
        expect(newState.isModalWishListOpen).toBe(true);
    })
    it('confirmOrder', () => {
        const basketCandidate = 1
        const action = confirmOrder(basketCandidate);
        const newState = mainReducer(initialState ,action);
        expect(newState.isModalBuyOpen).toBe(true);
        expect(newState.basket).toBe(1)
    })
    it('changeFav', () => {
        let newWishList = 1;
        const action = changeFav(newWishList);
        const newState = mainReducer(initialState ,action);
        expect(newState.isModalWishListOpen).toBe(false);
        expect(newState.arrWishList.indexOf(newWishList)).not.toBe(1)
    })
    it('getProducts', () => {
        const stateForProducts = {
            ...initialState,
            products: ["product"]
        }
        const action = getProducts(["product"]);
        const newState = mainReducer(stateForProducts ,action);
        expect(newState.products.length).toBe(1)
        expect(newState.products[0]).toBe("product")
    })
    it('finishOrder', () => {
        const action = finishOrder();
        const newState = mainReducer(initialState ,action);
        expect(newState.basket).toBe(0);
        expect(newState.arrBasketList).toEqual([]);
        expect(newState.basketCandidate).toBe(null);
    })
})