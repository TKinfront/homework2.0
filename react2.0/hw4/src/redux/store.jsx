import mainReducer from "./reducers/mainReducer";
import {createStore, combineReducers} from 'redux'

const reducers = combineReducers({
    mainReducer: mainReducer,
})

const store = createStore(reducers)

export default store;