import {
    openBuyModal,
    openWishListModal,
    confirmOrder,
    confirmWishList,
    changeFav,
    getProducts,
    finishOrder
} from './redux/reducers/mainReducer'
import {connect} from 'react-redux';
import App from './App';

const mapStateToProps = (state) => ({
        state: {
            ...state.mainReducer
        }
})

const mapDispatchToProps = {
    openBuyModal,
    openWishListModal,
    confirmOrder,
    confirmWishList,
    changeFav,
    getProducts,
    finishOrder
}

export default connect(mapStateToProps, mapDispatchToProps)(App);