import axios from 'axios'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useEffect } from 'react';

import Main from './components/Main/Main';
import Modal from './components/Modal/Modal';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import ModalWishList from './components/Modal/ModalWishList';
import Basket from './components/Basket/Basket';
import WishList from './components/WishList/WishList';



const App = (props) => {
console.log(props.state.basket);
  const fix = props.getProducts
  useEffect(() => {
    
    axios.get("/collection.json").then(res => {
      fix(res.data);
      console.log(res.data);
    })
      .catch(err => {
        console.log(err);
      })
  }, [fix])
 

  const changeFav = (id) => {
    props.changeFav(id);
  }

  const buttons = {
    headerBuyModal: "Зробіть Ваш вибір.",
    headerWishListModal: "Список обраних товарів.",
    bodyBuyModal: "Додати до кошику?",
    bodyWishListModal: "Додати до обраних?"
  }
  const openBuyModal = (idCandidate) => {
    localStorage.setItem("basketCandidate", idCandidate)
    props.openBuyModal(idCandidate)
  }


  const openWishListModal = (idCandidate) => {
    localStorage.setItem("wishCandidate", idCandidate)
    props.openWishListModal(idCandidate)
  }

  const confirmOrder = () => {
    localStorage.setItem("basket", props.state.basket + 1)
    props.confirmOrder()
  }

  const confirmWishList = () => {
    localStorage.setItem("wishList", props.state.wishList + 1)
    props.confirmWishList()
  }

  return (
    <BrowserRouter>

      <div className="app">
        <Header
          wishList={props.state.arrWishList}
          basket={props.state.basket}
        />
        <Routes>
          <Route path='/'
            element={<Main
              items={props.state.products}
              openBuyModal={openBuyModal}
              openWishListModal={openWishListModal}
              confirmWishList={confirmWishList}
              arrWishList={props.state.arrWishList}
              arrBasketList={props.state.arrBasketList}
              changeFav={changeFav}
            />
            }
          />
          <Route path='/basket' element={<Basket
            openWishListModal={openWishListModal}
            openBuyModal={openBuyModal}
            arrWishList={props.state.arrWishList}
            arrBasketList={props.state.arrBasketList}
            products={props.state.products}
            confirmWishList={confirmWishList}
            confirmOrder={confirmOrder}
            changeFav={changeFav}
          />} />
          <Route path='/wishlist' element={<WishList
            openWishListModal={openWishListModal}
            openBuyModal={openBuyModal}
            arrWishList={props.state.arrWishList}
            arrBasketList={props.state.arrBasketList}
            products={props.state.products}
            confirmWishList={confirmWishList}
            confirmOrder={confirmOrder}
            changeFav={changeFav}
          />} />
        </Routes>
        <Footer />
        {props.state.isModalBuyOpen === true ? <Modal buttons={buttons} openBuyModal={openBuyModal} confirmOrder={confirmOrder} /> : null}
        {props.state.isModalWishListOpen === true ? <ModalWishList buttons={buttons} openWishListModal={openWishListModal} confirmWishList={confirmWishList} /> : null}
      </div>

    </BrowserRouter>
  );
}


export default App;