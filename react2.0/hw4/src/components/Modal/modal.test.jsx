import Modal from "./Modal";
import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react'
import userEvent from "@testing-library/user-event";

describe('modal test', () => {
    it('click test', () => {
        const onClick = jest.fn();
        render(
            <Modal
                confirmOrder={onClick}
                buttons={() => { }}
                openBuyModal={() => { }}
            />
        )
        const btnBuy = screen.getByText('Підтвердити')
        userEvent.click(btnBuy)
        expect(onClick).toHaveBeenCalledTimes(1);

    })
    it('render name', () => {
        render(
            <Modal
                confirmOrder={() => { }}
                buttons={() => { }}
                openBuyModal={() => { }}
            />
        )
        expect(screen.getByText('Підтвердити')).toBeInTheDocument()
        expect(screen.getByText('Назад')).toBeInTheDocument()
    })
    it('modal snapshot', () => {
        let container
        container = render(<Modal
            confirmOrder={() => { }}
            buttons={() => { }}
            openBuyModal={() => { }}
            data-testid='test' />);
        expect(container).toMatchSnapshot();
    })
})