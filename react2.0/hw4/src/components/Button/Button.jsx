const Button = ({click, text, classBtn}) => {
    return (
        <button className={classBtn} onClick={click} type="button">
        {text}
        </button>
      );
}

export default Button;