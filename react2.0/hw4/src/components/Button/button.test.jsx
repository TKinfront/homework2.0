import Button from "./Button";
import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react'
import userEvent from "@testing-library/user-event"

describe('button test', () => {
    it('render button', () => {
        
        render (
            <Button
            text='Купити'
            />
        )
        expect(screen.getByText('Купити')).toBeInTheDocument();
    })
    it('click test', () => {
        const onClick = jest.fn();
        render(<Button
            text='Купити'
            click={onClick}
        />);
        const btnClick = screen.getByText('Купити');
        userEvent.click(btnClick)
        expect(onClick).toHaveBeenCalledTimes(1)
    })
    it('button snapshot', () => {
        let container
        container = render(<Button data-testid='test'/>);
        expect(container).toMatchSnapshot();
    })
    
})