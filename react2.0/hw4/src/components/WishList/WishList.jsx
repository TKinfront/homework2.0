import React from "react";
import Products from "../Main/Products/Products";
import style from "../Main/Main.module.scss"

const WishList = (props) => {
    let wishProducts = props.products.filter((product) => {
        return props.arrWishList.indexOf(product.id) !== -1;
    });

    const items = wishProducts.map((product, index) => {
        return (
            <Products
                key={index}
                name={product.name}
                price={product.price}
                picture={product.picture}
                id={product.id}
                lang={product.lang}
                openWishListModal={props.openWishListModal}
                openBuyModal={props.openBuyModal}
                arrWishList={props.arrWishList}
                confirmWishList={props.confirmWishList}
                confirmOrder={props.confirmOrder}
                wishStatus={props.arrWishList.includes(product.id)}
                changeFav={() => props.changeFav(product.id)}
            />
        );
    });

    return <div className={style.main}>{items}</div>;
};

export default WishList;