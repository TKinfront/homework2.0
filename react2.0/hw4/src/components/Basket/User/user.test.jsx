import User from "./User";
import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react'
import userEvent from "@testing-library/user-event"
import { Provider } from "react-redux";
import store from "../../../redux/store";

describe('Form confirmation', () => {
    it('btn test', () => {
        render(
            <Provider store={store}>
                <User
                />
            </Provider>
        )
        expect(screen.getByText('Оформити замовлення')).toBeInTheDocument();

    })
})