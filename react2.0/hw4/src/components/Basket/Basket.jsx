import React from "react";
import Products from "../Main/Products/Products";
import style from "../Main/Main.module.scss"
import User from "./User/User";

const Basket = (props) => {
    let basketProducts = props.products.filter((product) => {
        return props.arrBasketList.indexOf(product.id) !== -1;
    });

    const items = basketProducts.map((product, index) => {
        return (
            <Products
                key={index}
                name={product.name}
                price={product.price}
                picture={product.picture}
                id={product.id}
                lang={product.lang}
                openWishListModal={props.openWishListModal}
                openBuyModal={props.openBuyModal}
                arrWishList={props.arrWishList}
                confirmWishList={props.confirmWishList}
                confirmOrder={props.confirmOrder}
                wishStatus={props.arrWishList.includes(product.id)}
                changeFav={() => props.changeFav(product.id)}
            />
        );
    });

    return (
        <div>
            <div className={style.main}>{items}</div>
            <User />
        </div>
    )
}


export default Basket;