import style from "./Header.module.scss"
import basket from "./shopping-cart.png"
import wishList from "./wishlist.png"
import logo from "./logo.png"

import { NavLink } from "react-router-dom"

const Header = (props) => {
    return (
        <div className={style.header}>
            <div className={style.header_logoContainer}>
                <NavLink to="/" className={style.header_logoContainer_img}>
                    <img src={logo} alt="Logo" />
                    </NavLink>
            </div>
        <div className={style.container}>
            <div className={style.container_icons}>
                <NavLink to="/wishlist" className={style.container_icons_wishList}>
                    <img src={wishList} alt="wishList"/>
                    <div className={style.container_icons_wishList_counter}>{props.wishList.length}</div>
                </NavLink>
                <NavLink to="/basket" className={style.container_icons_basket}>
                    <img src={basket} alt="basket"/>
                    <div className={style.container_icons_basket_counter}>{props.basket}</div>
                </NavLink>
            </div>
        </div>
        </div>
    )
}
export default Header;