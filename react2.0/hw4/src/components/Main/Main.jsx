import style from "./Main.module.scss"
import Products from "./Products/Products";

const Main = (props) => {
    const ProductsCollection = (props.items.map(
        (item, index) => 
        <Products
        key={index}
        name={item.name}
        price={item.price}
        picture={item.picture}
        id={item.id}
        lang={item.lang}
        openBuyModal={props.openBuyModal}
        openWishListModal={props.openWishListModal}
        arrWishList={props.arrWishList}
        confirmWishList={props.confirmWishList}
        wishStatus={props.arrWishList.includes(item.id)}
        changeFav={() => props.changeFav(item.id)}
        />
    ))
    return (
        <div className={style.main}>
            {ProductsCollection}
        </div>
    )
}

export default Main;