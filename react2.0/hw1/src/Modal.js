import React from 'react';
import './App.scss';
import style from './Modal.module.scss';

class Modal extends React.Component {

  render() {
    const { header, closeButton, text, actions, modalHeader, modalBody, modalActions } = this.props;
    return (
      <div className={style.modal} onClick={this.props.onClose}>
        <div className={style.modalContent} onClick={(event) => {event.stopPropagation()}}>
          <div className={modalHeader}>
            <h2>{header}</h2>
            {closeButton && <span className={style.close} onClick={this.props.onClose}><svg fill='white'height="1.5em" viewBox="10 10 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg"><path d="M557.2 512l136.4-136.4c12.4-12.4 12.4-32.8 0-45.2-12.4-12.4-32.8-12.4-45.2 0L512 466.8l-136.4-136.4c-12.4-12.4-32.8-12.4-45.2 0-6.2 6.2-9.4 14.4-9.4 22.6 0 8.2 3.2 16.4 9.4 22.6l136.4 136.4-136.4 136.4c-6.2 6.2-9.4 14.4-9.4 22.6 0 8.2 3.2 16.4 9.4 22.6 12.4 12.4 32.8 12.4 45.2 0l136.4-136.4 136.4 136.4c12.4 12.4 32.8 12.4 45.2 0 12.4-12.4 12.4-32.8 0-45.2L557.2 512z"  /></svg></span>}
          </div>
          <div className={modalBody}>
            <p>{text}</p>
          </div>
          <div className={modalActions}>
            {actions}
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;