import React from 'react';
import './App.scss';
import Button from './Button';
import Modal from './Modal';
import style from './Modal.module.scss'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isModal1Open: false,
      isModal2Open: false,
    };
  }

  Modal1 = () => {
    this.setState({
      isModal1Open: !this.state.isModal1Open
    });
  }

  Modal2 = () => {
    this.setState({
      isModal2Open: !this.state.isModal2Open
    });
  }

  render() {
    return (
      <div className="app">
        <Button
          backgroundColor="#ff0000"
          text="Open first modal"
          onClick={this.Modal1}
        />
        <Button
          backgroundColor="#00ff00"
          text="Open second modal"
          onClick={this.Modal2}
        />
        {this.state.isModal1Open && (
          <Modal
            modalHeader={style.modalHeader1}
            modalBody={style.modalBody1}
            modalActions={style.modalActions1}
            header="First Modal"
            closeButton={true}
            text="This is the first modal."
            actions={<button className="btnClose1" onClick={this.Modal1}>Close</button>}
            onClose={this.Modal1}

          />
        )}
        {this.state.isModal2Open && (
          <Modal
            modalHeader={style.modalHeader2}
            modalBody={style.modalBody2}
            modalActions={style.modalActions2}
            header="Second Modal"
            closeButton={true}
            text="This is the second modal."
            actions={<button className="btnClose2" onClick={this.Modal2}>Close</button>}
            onClose={this.Modal2}

          />
        )}
      </div>
    );
  }
}

export default App;