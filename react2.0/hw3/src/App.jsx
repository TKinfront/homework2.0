import axios from 'axios'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from 'react';

import Main from './components/Main/Main';
import Modal from './components/Modal/Modal';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import ModalWishList from './components/Modal/ModalWishList';
import Basket from './components/Basket/Basket';
import WishList from './components/WishList/WishList';



const App = (props) => {
  const [state, setState] = useState({
    isModalBuyOpen: false,
    isModalWishListOpen: false,
    products: [],
    arrWishList: JSON.parse(localStorage.getItem('wishList')) || [],
    arrBasketList: [],
    basket: 0,
    wishList: 0,
    wishCandidate: null,
    basketCandidate: null
  })

  useEffect(() => {
    console.log(state);
    axios("/collection.json").then(res => {
      setState(prevState => ({ ...prevState, products: [...res.data] }));
    })
      .catch(err => {
        console.log(err);
      })
  }, [state])

  const changeFav = (id) => {

    setState(prevState => {

      const { arrWishList } = prevState

      let newWishList
      if (arrWishList.includes(id)) {
        newWishList = arrWishList.filter(wishId => wishId !== id);
      } else {
        newWishList = [...arrWishList, id];
      }
      localStorage.setItem('wishList', JSON.stringify(newWishList));
      return {
        ...prevState,
        arrWishList: newWishList
      }
    })
  }
  const buttons = {
    headerBuyModal: "Зробіть Ваш вибір.",
    headerWishListModal: "Список обраних товарів.",
    bodyBuyModal: "Додати до кошику?",
    bodyWishListModal: "Додати до обраних?"
  }
  const openBuyModal = (idCandidate) => {
    localStorage.setItem("basketCandidate", idCandidate)
    setState({
      ...state,
      isModalBuyOpen: state.isModalBuyOpen === false ? true : false,
      basketCandidate: idCandidate
    })
  }


  const openWishListModal = (idCandidate) => {
    localStorage.setItem("wishCandidate", idCandidate)
    setState({
      ...state,
      isModalWishListOpen: state.isModalWishListOpen === false ? true : false,
      wishCandidate: idCandidate
    })
  }

  const confirmOrder = () => {
    localStorage.setItem("basket", state.basket + 1)
    setState({
      ...state,
      basket: state.basket + 1,
      isModalBuyOpen: state.isModalBuyOpen === false ? true : false,
      arrBasketList: [...state.arrBasketList, localStorage.getItem("basketCandidate")]
    })
  }

  const confirmWishList = () => {
    localStorage.setItem("wishList", state.wishList + 1)
    setState({
      ...state,
      wishList: state.wishList + 1,
      isModalWishListOpen: state.isModalWishListOpen === false ? true : false,
      arrWishList: [...state.arrWishList, localStorage.getItem("wishCandidate")]
    })
  }

  return (
    <BrowserRouter>

      <div className="app">
        <Header
          wishList={state.arrWishList}
          basket={state.basket}
        />
        <Routes>
          <Route path='/'
            element={<Main
              items={state.products}
              openBuyModal={openBuyModal}
              openWishListModal={openWishListModal}
              confirmWishList={confirmWishList}
              arrWishList={state.arrWishList}
              arrBasketList={state.arrBasketList}
              changeFav={changeFav}
            />
            }
          />
          <Route path='/basket' element={<Basket
            openWishListModal={openWishListModal}
            openBuyModal={openBuyModal}
            arrWishList={state.arrWishList}
            arrBasketList={state.arrBasketList}
            products={state.products}
            confirmWishList={confirmWishList}
            confirmOrder={confirmOrder}
            changeFav={changeFav}
          />} />
          <Route path='/wishlist' element={<WishList
            openWishListModal={openWishListModal}
            openBuyModal={openBuyModal}
            arrWishList={state.arrWishList}
            arrBasketList={state.arrBasketList}
            products={state.products}
            confirmWishList={confirmWishList}
            confirmOrder={confirmOrder}
            changeFav={changeFav}
          />} />
        </Routes>
        <Footer />
        {state.isModalBuyOpen === true ? <Modal buttons={buttons} openBuyModal={openBuyModal} confirmOrder={confirmOrder} /> : null}
        {state.isModalWishListOpen === true ? <ModalWishList buttons={buttons} openWishListModal={openWishListModal} confirmWishList={confirmWishList} /> : null}
      </div>

    </BrowserRouter>
  );
}


export default App;