import React from "react";
import PropTypes from "prop-types";
import Products from "../Main/Products/Products";
import style from "../Main/Main.module.scss"

const WishList = (props) => {
    let wishProducts = props.products.filter((product) => {
        return props.arrWishList.indexOf(product.id) !== -1;
    });

    const items = wishProducts.map((product, index) => {
        return (
            <Products
                key={index}
                name={product.name}
                price={product.price}
                picture={product.picture}
                id={product.id}
                lang={product.lang}
                openWishListModal={props.openWishListModal}
                openBuyModal={props.openBuyModal}
                arrWishList={props.arrWishList}
                confirmWishList={props.confirmWishList}
                confirmOrder={props.confirmOrder}
                wishStatus={props.arrWishList.includes(product.id)}
                changeFav={() => props.changeFav(product.id)}
            />
        );
    });

    return <div className={style.main}>{items}</div>;
};

WishList.propTypes = {
    products: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string.isRequired,
            price: PropTypes.number.isRequired,
            picture: PropTypes.string.isRequired,
            id: PropTypes.string.isRequired,
            lang: PropTypes.string.isRequired,
        })
    ).isRequired,
    arrWishList: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
    openWishListModal: PropTypes.func.isRequired,
    openBuyModal: PropTypes.func.isRequired,
    confirmWishList: PropTypes.func.isRequired,
    confirmOrder: PropTypes.func.isRequired,
    changeFav: PropTypes.func.isRequired,
};

WishList.defaultProps = {
    products: [],
    arrWishList: [],
};

export default WishList;