import React from "react";
import PropTypes from "prop-types";
import style from "./Main.module.scss"
import Products from "./Products/Products";

const Main = (props) => {
    const ProductsCollection = (props.items.map(
        (item, index) =>
            <Products
                key={index}
                name={item.name}
                price={item.price}
                picture={item.picture}
                id={item.id}
                lang={item.lang}
                openBuyModal={props.openBuyModal}
                openWishListModal={props.openWishListModal}
                arrWishList={props.arrWishList}
                confirmWishList={props.confirmWishList}
                wishStatus={props.arrWishList.includes(item.id)}
                changeFav={() => props.changeFav(item.id)}
            />
    ))
    return (
        <div className={style.main}>
            {ProductsCollection}
        </div>
    )
}

Main.propTypes = {
    items: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string.isRequired,
            price: PropTypes.number.isRequired,
            picture: PropTypes.string.isRequired,
            id: PropTypes.string.isRequired,
            lang: PropTypes.string.isRequired,
        })
    ).isRequired,
    openBuyModal: PropTypes.func.isRequired,
    openWishListModal: PropTypes.func.isRequired,
    arrWishList: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
    confirmWishList: PropTypes.func.isRequired,
    changeFav: PropTypes.func.isRequired,
};

Main.defaultProps = {
    items: [],
};

export default Main;