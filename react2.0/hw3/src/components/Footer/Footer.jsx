import React from "react";
import style from "./Footer.module.scss"

const Footer = () => {
    return (
        <div className={style.container}>© Magazin 1991-2023. Усі права захищено.</div>
    )
}

Footer.defaultProps = {};

export default Footer