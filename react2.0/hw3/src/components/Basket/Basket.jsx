import React from "react";
import PropTypes from "prop-types";
import Products from "../Main/Products/Products";
import style from "../Main/Main.module.scss"
import User from "./User/User";


const Basket = (props) => {
    let basketProducts = props.products.filter((product) => {
        return props.arrBasketList.indexOf(product.id) !== -1;
    });

    const items = basketProducts.map((product, index) => {
        return (
            <Products
                key={index}
                name={product.name}
                price={product.price}
                picture={product.picture}
                id={product.id}
                lang={product.lang}
                openWishListModal={props.openWishListModal}
                openBuyModal={props.openBuyModal}
                arrWishList={props.arrWishList}
                confirmWishList={props.confirmWishList}
                confirmOrder={props.confirmOrder}
                wishStatus={props.arrWishList.includes(product.id)}
                changeFav={() => props.changeFav(product.id)}
            />
        );
    });

    return (
        <div>
            <div className={style.main}>{items}</div>
            <User />
        </div>
    )
}

Basket.propTypes = {
    products: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        picture: PropTypes.string.isRequired,
        lang: PropTypes.string.isRequired,
      })
    ).isRequired,
    arrBasketList: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
    openWishListModal: PropTypes.func.isRequired,
    openBuyModal: PropTypes.func.isRequired,
    arrWishList: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
    confirmWishList: PropTypes.func.isRequired,
    confirmOrder: PropTypes.func.isRequired,
    changeFav: PropTypes.func.isRequired,
  };

  Basket.defaultProps = {
    products: [],
    arrBasketList: [],
    arrWishList: [],
  };

export default Basket;