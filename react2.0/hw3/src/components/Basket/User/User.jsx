import style from './User.module.scss'
import { useFormik } from 'formik'
import { PatternFormat } from "react-number-format"
import * as Yup from "yup"

const User = () => {
    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            age: '',
            address: '',
            email: '',
            phone: ''
        },
        onSubmit: (values, { resetForm }) => {
            console.log("Форма відправлена", values);
        },
        validationSchema: Yup.object({
            firstName: Yup.string()
                .required("Будь-ласка введіть Ваше імʼя")
                .min(2, "Мінімум 2 символи")
                .max(30, "Максимум 30 символів"),
            lastName: Yup.string()
                .required("Будь-ласка введіть Ваше прізвище")
                .min(2, "Мінімум 2 символи")
                .max(30, "Максимум 30 символів"),
            age: Yup.number()
                .required("Будь-ласка введіть Вашу вік")
                .min(2, "Мінімум 2 символи")
                .max(30, "Максимум 30 символів"),
            address: Yup.string()
                .required("Будь-ласка введіть Вашу адресу")
                .min(2, "Мінімум 2 символи")
                .max(100, "Максимум 100 символів"),
            email: Yup.string().email()
                .required("Будь-ласка введіть Вашу електронну пошту")
                .min(2, "Мінімум 2 символи")
                .max(50, "Максимум 50 символів"),
            phone: Yup.string()
                .required("Будь-ласка введіть Ваш телефон")

        })
    });
    return (
        <>
        <div className={style.pageContainer}>
            <div className={style.h1}>Заповніть форму для отримання Вашого замовлення</div>
            <form
                onSubmit={formik.handleSubmit}
                autoComplete='off'
                noValidate
            >

                <div className={style.formContainer}>
                    <div className={style.formContainer2}>
                        <div>
                            <label htmlFor="firstName">Імʼя</label>
                            <input
                                placeholder='Введіть Ваше імʼя'
                                name="firstName"
                                id="firstName"
                                type="text"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.firstName}
                            />
                            {formik.touched.firstName && formik.errors.firstName ? (
                                <div className={style.error}>{formik.errors.firstName}</div>
                            ) : null}
                        </div>
                        <div>
                            <label htmlFor="lastName">Прізвище</label>
                            <input
                                placeholder='Введіть Ваше прізвище'
                                name="lastName"
                                id="lastName"
                                type="text"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.lastName}
                            />
                            {formik.touched.lastName && formik.errors.lastName ? (
                                <div className={style.error}>{formik.errors.lastName}</div>
                            ) : null}
                        </div>
                        <div>
                            <label htmlFor="age">Вік</label>
                            <input
                                placeholder='Вкажіть Ваш вік'
                                name="age"
                                id="age"
                                type="number"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.age} />
                            {formik.touched.age && formik.errors.age ? (
                                <div className={style.error}>{formik.errors.age}</div>
                            ) : null}
                        </div>
                        <div>
                            <label htmlFor="address">Адреса</label>
                            <input
                                placeholder='Напишіть Вашу адресу'
                                name="address"
                                id="address"
                                type="text"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.address}
                            />
                            {formik.touched.address && formik.errors.address ? (
                                <div className={style.error}>{formik.errors.address}</div>
                            ) : null}
                        </div>
                        <div>
                            <label htmlFor="email">Email</label>
                            <input
                                placeholder='Напишіть ваш email'
                                name="email"
                                id="email"
                                type="text"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.email}
                            />
                            {formik.touched.email && formik.errors.email ? (
                                <div className={style.error}>{formik.errors.email}</div>
                            ) : null}
                        </div>
                        <div>
                            <label htmlFor="phone">Телефон</label>
                            <PatternFormat
                                format="+38 (###) ###-##-##"
                                allowEmptyFormatting mask="_"
                                name="phone"
                                id="phone"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.phone}
                            />
                            {formik.touched.phone && formik.errors.phone ? (
                                <div className={style.error}>{formik.errors.phone}</div>
                            ) : null}
                        </div>

                    </div>
                    <div>
                        <button type="submit">Оформити замовлення</button>
                    </div>
                </div>
            </form>
            </div>
        </>
    )
}

export default User;

