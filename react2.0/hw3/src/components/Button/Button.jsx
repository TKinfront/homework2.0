import React from "react";
import PropTypes from "prop-types";

const Button = ({click, text, classBtn}) => {
    return (
        <button className={classBtn} onClick={click} type="button">
        {text}
        </button>
      );
}

Button.propTypes = {
  click: PropTypes.func.isRequired,
  classBtn: PropTypes.string,
};

Button.defaultProps = {
  classBtn: "",
};

export default Button;